package invokers;

import commands.Command;
import core.UndoRedoStack;
import momento.Momento;

public class Button extends AbstractInvoker implements Invoker {

	private UndoRedoStack momentoStack;
	
	public Button(UndoRedoStack momentoStack) {
		this.momentoStack = momentoStack;
	}

	@Override
	public void invoke(Command cmd) {
		cmd.execute();

		if (cmd.isUndoRedoable()) {
			momentoStack.pushDoneCommand(ctx.save());
		}
	}
}
