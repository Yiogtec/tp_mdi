package invokers;

import commands.Command;
import core.Context;
import momento.Momento;
import momento.Originator;

public abstract class AbstractInvoker implements Invoker {
	
	protected Context ctx;
	
	public AbstractInvoker() {
	}
	
	public void setContext(Context ctx) {
		this.ctx = ctx;
	}
	
	abstract public void invoke(Command command);
	
	
}
