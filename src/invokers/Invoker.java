package invokers;

import commands.Command;
import core.Context;

public interface Invoker {
		
	public void invoke(Command command);
	public void setContext(Context ctx);

}
