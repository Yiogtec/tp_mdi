import java.util.ArrayList;
import java.util.List;

import commands.Backward;
import commands.Copy;
import commands.Cut;
import commands.Forward;
import commands.Paste;
import core.CommandFactory;
import core.Context;
import core.UndoRedoStack;
import ihms.GraphicIHM;
import ihms.TextIHM;
import invokers.Button;
import invokers.Invoker;

public class Main {

	public static void main(String... args) {

		UndoRedoStack stack = new UndoRedoStack();
		
		List<String> commands = new ArrayList<String>();
		commands.add("Backward");
		commands.add("Forward");
		commands.add("Cut");
		commands.add("Copy");
		commands.add("Paste");
		commands.add("Undo");
		commands.add("Redo");
		
		Invoker invoker = new Button(stack);
		Context ctx = new Context(invoker, stack);
		invoker.setContext(ctx);
		
		CommandFactory cmdFact = new CommandFactory(ctx);
		
//		TextIHM.init(ctx, cmdFact, commands);
//		TextIHM.run();
		GraphicIHM.init(ctx, cmdFact, commands);
		GraphicIHM.run();
		
	}
	

}