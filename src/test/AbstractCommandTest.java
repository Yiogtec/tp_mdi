package test;

import org.junit.After;
import org.junit.Before;

import core.CommandFactory;
import core.Context;
import core.UndoRedoStack;
import invokers.Button;
import invokers.Invoker;

public abstract class AbstractCommandTest {
	protected Context ctx;
	protected CommandFactory cmdFact;
	protected final String oracle = "0123456789";
	
	@Before
	public void setUp() throws Exception {
		UndoRedoStack stack = new UndoRedoStack();
		Invoker invoker = new Button(stack);
		ctx = new Context(invoker, stack);
		
		invoker.setContext(ctx);
		
		cmdFact = new CommandFactory(ctx);
		ctx.setBufferContent(oracle);
	}

	@After
	public void tearDown() throws Exception {
		ctx = null;
		cmdFact = null;
	}
}
