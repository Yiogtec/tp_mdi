package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestBackwardForward extends AbstractCommandTest {

	@Test
	public void testBackwardLimit() {
		ctx.getInvoker().invoke(cmdFact.create("Backward"));
		assertEquals(ctx.renderSelection(), "");
		assertEquals((Integer)0, ctx.getSelection().getCursorPosition());
		assertEquals((Integer)0, ctx.getSelection().getLength());		
	}	

	@Test
	public void testForwardLimit() {
		for (int i = 0 ; i < oracle.length() ; i++) {
			ctx.getInvoker().invoke(cmdFact.create("Forward"));
		}
		assertEquals(oracle, ctx.renderSelection());
		assertEquals((Integer)0, ctx.getSelection().getCursorPosition());
		assertEquals((Integer)oracle.length(), ctx.getSelection().getLength());
		
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		
		assertEquals(oracle, ctx.renderSelection());
		assertEquals((Integer)oracle.length(), ctx.getSelection().getLength());
	}

	@Test
	public void testBackwardThenForward() {
		for (int i = 0 ; i < oracle.length() ; i++) {
			ctx.getInvoker().invoke(cmdFact.create("Backward"));
		}
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		
		assertEquals(oracle.substring(0, 2), ctx.renderSelection());
		assertEquals((Integer)2, ctx.getSelection().getLength());
	}

	@Test
	public void testForwardThenBackward() {
		for (int i = 0 ; i < oracle.length() ; i++) {
			ctx.getInvoker().invoke(cmdFact.create("Forward"));
		}
		ctx.getInvoker().invoke(cmdFact.create("Backward"));
		ctx.getInvoker().invoke(cmdFact.create("Backward"));
		
		assertEquals(oracle.substring(0, oracle.length()-2), ctx.renderSelection());
		assertEquals((Integer)8, ctx.getSelection().getLength());
	}
	
	@Test
	public void testRandomWithCursorMovement() {
		ctx.getInvoker().invoke(cmdFact.createMoveCursor(2));
		for (int i = 0 ; i < 5 ; i++) {
			ctx.getInvoker().invoke(cmdFact.create("Forward"));
		}
		ctx.getInvoker().invoke(cmdFact.create("Backward"));

		assertEquals((Integer)2, ctx.getSelection().getCursorPosition());
		assertEquals((Integer)4, ctx.getSelection().getLength());
		assertEquals(oracle.substring(2, 6), ctx.renderSelection());

		ctx.getInvoker().invoke(cmdFact.createMoveCursor(5));
		
		assertEquals("", ctx.renderSelection());
	}
	
	@Test
	public void testBackwardToStartSelection() {
		ctx.getInvoker().invoke(cmdFact.createMoveCursor(4));

		ctx.getInvoker().invoke(cmdFact.create("Backward"));
		ctx.getInvoker().invoke(cmdFact.create("Backward"));
		

		assertEquals((Integer)4, ctx.getSelection().getCursorPosition());
		assertEquals((Integer)(-2), ctx.getSelection().getLength());
		assertEquals(oracle.substring(2, 4), ctx.renderSelection());
	}
}
