package test;

import static org.junit.Assert.assertEquals;

import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.junit.Test;

public class TestCopy extends AbstractCommandTest {

	@Test
	public void testCopyWithoutSelection() throws HeadlessException, UnsupportedFlavorException, IOException {

		String beforeCopy = getClipboard();
		ctx.getInvoker().invoke(cmdFact.create("Copy"));	
		String afterCopy = getClipboard();

		assertEquals(beforeCopy, afterCopy);
	}
	
	@Test
	public void testCopy1CharSelected() throws HeadlessException, UnsupportedFlavorException, IOException {
		for(int i = 0; i < oracle.length(); i++ ){
			ctx.setSelection(i, 1); // from char i, selection size = 1
			ctx.getInvoker().invoke(cmdFact.create("Copy"));	

			String write = getClipboard();
			assertEquals(write,""+i);
		}
	}
	
	@Test
	public void testCopyChars() throws HeadlessException, UnsupportedFlavorException, IOException {
		String res = "";
		for(int i = 0; i < oracle.length(); i++ ){
			ctx.setSelection(0, i+1); // from char i, selection size = 1
			ctx.getInvoker().invoke(cmdFact.create("Copy"));	

			String write = getClipboard();
			res += i;
			assertEquals(write,res);
		}
	}	
		
	private String getClipboard() throws HeadlessException, UnsupportedFlavorException, IOException{
		String value = (String) Toolkit.getDefaultToolkit()
		        .getSystemClipboard().getData(DataFlavor.stringFlavor);
		
		return value;
	}

}
