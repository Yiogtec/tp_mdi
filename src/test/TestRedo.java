package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestRedo extends AbstractCommandTest {

	@Test
	public void testRedoWithoutUndo() {
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Cut"));
		ctx.getInvoker().invoke(cmdFact.createMoveCursor(7));
		ctx.getInvoker().invoke(cmdFact.create("Paste"));
		assertEquals("2345678019", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Redo"));
		assertEquals("2345678019", ctx.getBuffer().toString());
		
	}
	
	@Test
	public void testRedoWithUndoButAfterUndoableCommandExec() {
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Cut"));
		ctx.getInvoker().invoke(cmdFact.createMoveCursor(7));

		ctx.getInvoker().invoke(cmdFact.create("Undo"));
		assertEquals(oracle, ctx.getBuffer().toString());
		
		ctx.getInvoker().invoke(cmdFact.create("Paste"));
		assertEquals("012345601789", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Redo"));
		assertEquals("012345601789", ctx.getBuffer().toString());
		
	}
	
	
	@Test
	public void testSimpleRedo() {
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Cut"));
		ctx.getInvoker().invoke(cmdFact.createMoveCursor(7));
		ctx.getInvoker().invoke(cmdFact.create("Paste"));
		assertEquals("2345678019", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Undo"));
		assertEquals("23456789", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Redo"));
		assertEquals("2345678019", ctx.getBuffer().toString());
		
	}
	
	@Test
	public void testRedoOnTheSameCommand() {
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Cut"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Cut"));
		assertEquals("456789", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Undo"));
		assertEquals("23456789", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Undo"));
		assertEquals(oracle, ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Redo"));
		assertEquals("23456789", ctx.getBuffer().toString());
		

		ctx.getInvoker().invoke(cmdFact.create("Redo"));
		assertEquals("456789", ctx.getBuffer().toString());
	}

}
