package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestWrite extends AbstractCommandTest {

	@Test
	public void testInstantWrite() {
		ctx.getInvoker().invoke(cmdFact.createWrite("007"));
		assertEquals("0070123456789", ctx.getBuffer().toString());
	}
	
	@Test
	public void testWriteAfterSetCursor() {
		ctx.getInvoker().invoke(cmdFact.createMoveCursor(5));
		ctx.getInvoker().invoke(cmdFact.createWrite("007"));
		assertEquals("0123400756789", ctx.getBuffer().toString());
	}
	
	@Test
	public void testWriteBeforeAndAfterSetCursor() {
		ctx.getInvoker().invoke(cmdFact.createWrite("007"));
		ctx.getInvoker().invoke(cmdFact.createMoveCursor(5));
		ctx.getInvoker().invoke(cmdFact.createWrite("007"));
		assertEquals("0070100723456789", ctx.getBuffer().toString());
	}

}
