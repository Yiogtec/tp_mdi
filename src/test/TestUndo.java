package test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestUndo extends AbstractCommandTest {

	@Test
	public void testDoubleUndo() {
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Cut"));
		ctx.getInvoker().invoke(cmdFact.createMoveCursor(7));
		ctx.getInvoker().invoke(cmdFact.create("Paste"));
		assertEquals("2345678019", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Undo"));
		assertEquals("23456789", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Undo"));
		assertEquals("0123456789", ctx.getBuffer().toString());
		
	}
	
	@Test
	public void testUndoOnTheSameCommand() {
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Cut"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Forward"));
		ctx.getInvoker().invoke(cmdFact.create("Cut"));
		assertEquals("456789", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Undo"));
		assertEquals("23456789", ctx.getBuffer().toString());

		ctx.getInvoker().invoke(cmdFact.create("Undo"));
		assertEquals(oracle, ctx.getBuffer().toString());
		
	}

}
