package test;

import static org.junit.Assert.assertEquals;

import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.junit.Test;

public class TestPaste extends AbstractCommandTest {
	@Test
	public void testPasteWithEmptySelection() throws HeadlessException, UnsupportedFlavorException, IOException {
		// clear clipboard
		Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
		clpbrd.setContents(new StringSelection(""), null);
		
		ctx.setSelection(0, 0);
		ctx.getInvoker().invoke(cmdFact.create("Copy"));
		ctx.getInvoker().invoke(cmdFact.create("Paste"));
		
		assertEquals(oracle, ctx.getBuffer().toString());
	}
	
	@Test
	public void testPaste1CharSelected() throws HeadlessException, UnsupportedFlavorException, IOException {
		for(int i = 0; i < oracle.length() * 2 ; i+=2 ){
			ctx.setSelection(i, 1);
			ctx.getInvoker().invoke(cmdFact.create("Copy"));
			ctx.setSelection(i, 0);
			ctx.getInvoker().invoke(cmdFact.create("Paste"));
			
		}
		
		String oracleDouble = "00112233445566778899";
		assertEquals(oracleDouble, ctx.getBuffer().toString());
	}
	
	@Test
	public void testPasteChars() throws HeadlessException, UnsupportedFlavorException, IOException {
		ctx.setSelection(2, 5); 
		ctx.getInvoker().invoke(cmdFact.create("Copy"));
		ctx.setSelection(2, 0); 
		ctx.getInvoker().invoke(cmdFact.create("Paste"));
		
		String beforePasted = "01";
		String pasted = "23456";
		String afterPasted = "23456789";
		assertEquals(beforePasted + pasted + afterPasted, ctx.getBuffer().toString());
	}	
	
	@Test
	public void testPasteWithSelectionNotEmpty() throws HeadlessException, UnsupportedFlavorException, IOException {
		ctx.setSelection(2, 6); 
		ctx.getInvoker().invoke(cmdFact.create("Copy"));
		ctx.getInvoker().invoke(cmdFact.create("Paste"));
		
		assertEquals(oracle, ctx.getBuffer().toString());
	}

}
