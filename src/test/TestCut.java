package test;

import static org.junit.Assert.assertEquals;

import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import org.junit.Test;

public class TestCut extends AbstractCommandTest {

	@Test
	public void testCutWithoutSelection() throws HeadlessException, UnsupportedFlavorException, IOException {
		
		String beforeCopy = getClipboard();
		ctx.getInvoker().invoke(cmdFact.create("Cut"));		
		String afterCopy = getClipboard();
		
		assertEquals(beforeCopy, afterCopy);
		assertEquals(oracle, ctx.getBuffer().toString());
	}
	
	@Test
	public void testCutFirstCharSelected() throws HeadlessException, UnsupportedFlavorException, IOException {
		for(int i = 0; i < oracle.length(); i++ ){
			ctx.setSelection(0, 1);
			ctx.getInvoker().invoke(cmdFact.create("Cut"));		

			String write = getClipboard();
			assertEquals(write,""+i);
			assertEquals(oracle.substring(i+1, oracle.length()), ctx.getBuffer().toString());
		}
	}
	
	@Test
	public void testCutChars() throws HeadlessException, UnsupportedFlavorException, IOException {
		ctx.setSelection(2, 5);
		ctx.getInvoker().invoke(cmdFact.create("Cut"));		

		String write = getClipboard();
		assertEquals("23456", write);
		assertEquals("01789", ctx.getBuffer().toString());
	}
	
	@Test
	public void MultipleCut() throws HeadlessException, UnsupportedFlavorException, IOException {
		ctx.setSelection(2, 5);
		ctx.getInvoker().invoke(cmdFact.create("Cut"));		
		ctx.getInvoker().invoke(cmdFact.create("Cut"));		
		
		String write = getClipboard();
		assertEquals("23456", write);
		assertEquals("01789", ctx.getBuffer().toString());
		
		ctx.setSelection(2, 2);
		ctx.getInvoker().invoke(cmdFact.create("Cut"));		

		write = getClipboard();
		assertEquals("78", write);
		assertEquals("019", ctx.getBuffer().toString());
		
		
		
	}	
		
	private String getClipboard() throws HeadlessException, UnsupportedFlavorException, IOException{
		String value = (String) Toolkit.getDefaultToolkit()
		        .getSystemClipboard().getData(DataFlavor.stringFlavor);
		
		return value;
	}

}
