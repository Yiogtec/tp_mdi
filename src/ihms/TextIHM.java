package ihms;

import java.util.List;
import java.util.Scanner;

import commands.Command;
import core.CommandFactory;
import core.Context;

public class TextIHM implements IHM {

	private static Context ctx;
	private static Scanner sc;
	private static List<Command> commands;
	private static CommandFactory cmdFact;
	
	public static void init(Context ctx, CommandFactory cmdFact, List<String> commandList) {
		TextIHM.ctx = ctx;
		sc = new Scanner(System.in);
		TextIHM.cmdFact = cmdFact;
		TextIHM.commands = cmdFact.create(commandList);
	}
	
	private static void displayCommands() {
		System.out.println("******* Menu *******");
		System.out.println("0 - Leave");
		System.out.println("1 - Set Cursor Position");
		System.out.println("2 - write");
		int shift = 3;
		for (int i = 0 ; i < TextIHM.commands.size() ; i++) {
			Command cmd = TextIHM.commands.get(i);
			if (cmd.executable()) {
				System.out.println(i+shift +" - "+ cmd.getClass().getSimpleName());
			}
		}
		System.out.println("********************");
	}
	
	private static void displayContent() {
		System.out.println("Content: ");
		System.out.println("--------------------");
		System.out.println(TextIHM.ctx.getBuffer());
		System.out.println("--------------------");
	}
	
	public static void run() {
		displayCommands();
		catchAction();
	}
	
	private static void catchAction() {
		int choice = -1;

		System.out.println("Choice: ");
		while( (choice = TextIHM.sc.nextInt()) != 0) {
			TextIHM.sc.nextLine(); // throw away '\n'
			if(choice == 1){
				setCursor();
			} else if (choice == 2) {
				writeSomething();
			} else {
				int cmdChoice = choice-3;
				TextIHM.ctx.getInvoker().invoke(TextIHM.commands.get(cmdChoice));
				TextIHM.commands.set(cmdChoice, cmdFact.create(TextIHM.commands.get(cmdChoice).getClass().getSimpleName()));
				System.out.println(TextIHM.ctx.getBuffer());
			}
			displayCommands();
			displayContent();
			System.out.println("Choice: ");
		}
		TextIHM.sc.close();
	}
	
	private static void setCursor(){
		System.out.println("Cursor position: ");
		int position = TextIHM.sc.nextInt();
		TextIHM.sc.nextLine(); // throw away '\n'
		TextIHM.ctx.getInvoker().invoke(TextIHM.cmdFact.createMoveCursor(position));
	}
	
	private static void writeSomething(){
		System.out.println("Input text: ");
		String text = TextIHM.sc.nextLine();
		TextIHM.ctx.getInvoker().invoke(TextIHM.cmdFact.createWrite(text));
	}

}
