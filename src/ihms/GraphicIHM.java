package ihms;

import java.util.List;
import java.util.function.UnaryOperator;

import commands.Command;
import core.CommandFactory;
import core.Context;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextFormatter.Change;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.converter.IntegerStringConverter;

public class GraphicIHM extends Application implements IHM  {
	
	private static Context context;
	private static List<Command> commands;
	private static CommandFactory cmdFactory;
	private static Group root;
	private static Label textBuffer;
	private static Text textSelection;
	private static int idCmd;
	
	public static void init(Context ctx, CommandFactory cmdFact, List<String> commandList){
		GraphicIHM.context = ctx;
		GraphicIHM.cmdFactory = cmdFact;
		GraphicIHM.commands = cmdFact.create(commandList);
		
		root = new Group();
		
	}
	
	public static void run() {
		launch();
	}

	@Override
	public void start(Stage stage) throws Exception {
		// TODO Auto-generated method stub
        stage.setWidth(800);
        stage.setHeight(600);
        stage.setTitle("Editeur MDI");
        stage.getIcons().add(new Image("mocassins.jpg"));
        
        
        Scene scene = new Scene(GraphicIHM.root);
        scene.setFill(Color.DARKBLUE);
        
        ScrollPane editor = new ScrollPane();
        editor.setTranslateX(205);
        editor.setTranslateY(20);
        editor.setMaxWidth(550);
        editor.setPrefSize(550, 400);
        editor.setBackground((new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY))));
		GraphicIHM.textBuffer = new Label();
		GraphicIHM.textBuffer.setWrapText(true);
		GraphicIHM.textBuffer.setMaxWidth(500);
		GraphicIHM.textBuffer.setMaxSize(500, 400);
		GraphicIHM.textBuffer.setStyle("-fx-font-size: 15; -fx-text-fill: black;");
        editor.setContent(textBuffer);

		Group selectionPanel = new Group();
		selectionPanel.setTranslateX(200);
		selectionPanel.setTranslateY(500);
		GraphicIHM.textSelection = new Text(10, 0, "Selection : ");
		GraphicIHM.textSelection.setFont(new Font(20));
		GraphicIHM.textSelection.setFill(Color.WHITE);
		selectionPanel.getChildren().add(GraphicIHM.textSelection);
		GraphicIHM.root.getChildren().add(selectionPanel);
		GraphicIHM.root.getChildren().add(editor);

        this.displayCommands();
        this.displayContent();
		this.writeSomething();
		this.setCursor();
        stage.setScene(scene);
        stage.show();
	}

	public void displayCommands() {
        Group panel = new Group();
        panel.setTranslateX(0);
        panel.setTranslateY(70);
        Text text = new Text(35,0,"Action");
		text.setFont(new Font(20));
		text.setFill(Color.WHITE);
        panel.getChildren().add(text);
        
		for (int i = 0 ; i < GraphicIHM.commands.size() ; i++) {
			Command cmd = GraphicIHM.commands.get(i);
			Button button = new Button(cmd.getClass().getSimpleName());
		    button.setTranslateX(5);
		    button.setTranslateY((i*70)+20);
		    button.setPrefWidth(120);
		    button.setPrefHeight(40);
		    idCmd = i;
			if (cmd.executable()) {
		        button.setOnAction(new EventHandler<ActionEvent>() {
		            @Override public void handle(ActionEvent e) {
		            	GraphicIHM.context.getInvoker().invoke(cmd);
						GraphicIHM.commands.set(idCmd, GraphicIHM.cmdFactory.create(GraphicIHM.commands.get(idCmd).getClass().getSimpleName()));
						displayContent();
		            }
		        });
			}
			else{
				button.setDisable(true);
			}
			 panel.getChildren().add(button);
		}
		
		GraphicIHM.root.getChildren().add(panel);
		
	}
	
	private void displayContent() {
 
		GraphicIHM.textBuffer.setText(GraphicIHM.context.getBuffer().toString());
		displayCommands();
		displaySelection();
		
	}
	
	private void writeSomething(){
		
		Group writePanel = new Group();
        writePanel.setTranslateX(200);
        writePanel.setTranslateY(400);
		TextField field = new TextField();
		field.setTranslateX(5);
		field.setTranslateY(25);
		field.setPrefWidth(500);
		
		Button button = new Button("Write");
		button.setTranslateX(510);
		button.setTranslateY(25);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
        		String text = field.getText();
        		GraphicIHM.context.getInvoker().invoke(GraphicIHM.cmdFactory.createWrite(text));
        		field.setText("");
        		displayContent();
                
            }
        });
		writePanel.getChildren().add(field);
		writePanel.getChildren().add(button);
		GraphicIHM.root.getChildren().add(writePanel);
	}
	
	private void setCursor(){

		Group cursorPanel = new Group();
		cursorPanel.setTranslateX(200);
		cursorPanel.setTranslateY(500);
		TextField field = new TextField();
		field.setTranslateX(10);
		field.setTranslateY(10);
		field.setPrefWidth(40);
		UnaryOperator<Change> integerFilter = change -> {
		    String newText = change.getControlNewText();
		    if (newText.matches("-?(([0-9]|[1-9][0-9])*)?")) { 
		        return change;
		    }
		    return null;
		};

		field.setTextFormatter(
		    new TextFormatter<Integer>(new IntegerStringConverter(), 0, integerFilter));
		
		Text text = new Text(150, 30, "Cursor : "+0);
		text.setFont(new Font(20));
		text.setFill(Color.WHITE);
		Button button = new Button("Move cursor");
		button.setTranslateX(60);
		button.setTranslateY(10);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
        		int position = Integer.parseInt(field.getText());
        		GraphicIHM.context.getInvoker().invoke(GraphicIHM.cmdFactory.createMoveCursor(position));
        		text.setText("Cursor : "+ position);
                
            }
        });
		cursorPanel.getChildren().add(field);
		cursorPanel.getChildren().add(button);
		cursorPanel.getChildren().add(text);
		GraphicIHM.root.getChildren().add(cursorPanel);
	}

	public void  displaySelection(){
		GraphicIHM.textSelection.setText("Selection : "+GraphicIHM.context.renderSelection());

	}
}
