package momento;

public interface Originator {

	public Momento save();
	public void restore(Momento m);
}
