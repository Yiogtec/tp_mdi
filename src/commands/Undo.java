package commands;

import core.Context;
import momento.Momento;

public class Undo extends BufferCommand {

	
	public Undo(Context ctx) {
		super(ctx);
	}

	@Override
	public void execute(){
		if (executable()) {
			Momento m = getContext().getUndoRedoStack().undo();
			getContext().restore(m);
		}
	}

	@Override
	public boolean executable() {
		return getContext().getUndoRedoStack().canUndo();
	}

}
