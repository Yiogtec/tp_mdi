package commands;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;

import core.Context;

public class Copy extends BufferCommand {

	
	public Copy(Context ctx) {
		super(ctx);
	}

	public void execute(){
		if (executable()) {
			String toCopy = getContext().renderSelection();
			StringSelection stringSelection = new StringSelection(toCopy);
			Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
			clpbrd.setContents(stringSelection, null);
		}
	}

	@Override
	public boolean executable() {
		return getContext().getSelection().getLength() > 0;
	}

}
