package commands;

public interface Command {

	public void execute();
	
	public boolean executable();
	
	public boolean isUndoRedoable();
}
