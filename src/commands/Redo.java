package commands;

import core.Context;
import momento.Momento;

public class Redo extends BufferCommand {

	public Redo(Context ctx) {
		super(ctx);
	}

	@Override
	public void execute(){
		if (executable()) {
			Momento m = getContext().getUndoRedoStack().redo();
			getContext().restore(m);
		}
	}

	@Override
	public boolean executable() {
		return getContext().getUndoRedoStack().canRedo();
	}

}
