package commands;

import core.Context;

public class Forward extends BufferCommand {

	public Forward(Context ctx) {
		super(ctx);
	}

	public void execute(){
		if (executable()) {
			int newLength = getContext().getSelection().getLength() + 1;
			getContext().setSelection(getContext().getCursorPosition(), newLength);
		}
	}

	@Override
	public boolean executable() {
		int newLength = getContext().getSelection().getLength() + 1;
		int secondBound = getContext().getCursorPosition() + newLength;
		
		return secondBound >= 0 && secondBound <= getContext().getBuffer().length();
	}
}
