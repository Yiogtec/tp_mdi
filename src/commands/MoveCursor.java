package commands;

import core.Context;

public class MoveCursor extends BufferCommand {

	int position;
	
	public MoveCursor(Context ctx, int position) {
		super(ctx);
		this.position= position;
	}

	@Override
	public void execute() {
		//System.out.println("execute movecursor");
		if (executable()) {
			//System.out.println("set position to : " + position);
			getContext().getSelection().setCursorPosition(position);
		}
	}

	@Override
	public boolean executable() {
		return position >= 0 && position <= getContext().getBuffer().length();
	}

}
