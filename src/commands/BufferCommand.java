package commands;

import core.Context;

public abstract class BufferCommand implements Command {

	protected Context ctx;
	
	public BufferCommand(Context ctx) {
		this.ctx = ctx;
	}

	public Context getContext() {
		return ctx;
	}
	
	public boolean isUndoRedoable() {
		return false;
	}
}
