package commands;

import core.Context;

public class Write extends BufferCommand {
	private String input;
	
	public Write(Context ctx, String input) {
		super(ctx);
		this.input = input;
	}

	@Override
	public void execute() {
		getContext().getBuffer().write(getContext().getCursorPosition(), input);
	}

	@Override
	public boolean executable() {
		return true;
	}
	
	
	public boolean isUndoRedoable() {
		return true;
	}

}
