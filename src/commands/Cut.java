package commands;

import core.Context;
import core.Pair;

public class Cut extends Copy {
	
	public Cut(Context ctx) {
		super(ctx);
	}

	public void execute(){
		super.execute();
		Pair<Integer, Integer> p = getContext().getSelection().getStartAndEnd();
		ctx.getBuffer().remove(p.getFirst(), p.getSecond());
		ctx.setSelection(ctx.getCursorPosition(), 0);
	}
	
	public boolean isUndoRedoable() {
		return true;
	}
}
