package commands;

import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

import core.Context;

public class Paste extends BufferCommand {
	
	public Paste(Context ctx) {
		super(ctx);
	}

	public void execute() {
		try {
			String write = (String) Toolkit.getDefaultToolkit()
			        .getSystemClipboard().getData(DataFlavor.stringFlavor);
			getContext().eraseSelection();
			getContext().getBuffer().write(getContext().getCursorPosition(), write);
		} catch (HeadlessException | UnsupportedFlavorException | IOException e) {
			e.printStackTrace();
		} 
	}

	@Override
	public boolean executable() {
		return true;
	}
	
	public boolean isUndoRedoable() {
		return true;
	}
	
}
