package core;

import java.util.Stack;

import momento.Momento;

public class UndoRedoStack {

	private Stack<Momento> undoStack;
	private Stack<Momento> redoStack;
	
	public UndoRedoStack() {
		undoStack = new Stack<>();
		redoStack = new Stack<>();
	}
	
	public void pushDoneCommand(Momento cmd) {
		this.undoStack.push(cmd);
		this.redoStack.clear();
	}
	
	public Momento undo() {
		Momento m = null;
		if (canUndo()) {
			m = undoStack.pop();
			redoStack.push(m);
		}
		if(undoStack.size() == 0) {
			return null;
		}
		return undoStack.peek();
	}
	
	public Momento redo() {
		Momento m = null;
		if (canRedo()) {
			m = redoStack.pop();
			undoStack.push(m);
		}
		return m;
	}
	
	public boolean canRedo() {
		return redoStack.size() > 0;
	}
	
	public boolean canUndo() {
		return undoStack.size() > 0;
	}
	
}
