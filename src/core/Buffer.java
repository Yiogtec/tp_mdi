package core;

public class Buffer {
	
	private StringBuffer text;
	
//	/** first : include, second : exclude  example : [x, y [ **/
//	private Pair<Integer,Integer> selection; 

	public Buffer(){		
		text = new StringBuffer();
	}
	
	
	public void clear() {
		this.text.delete(0, this.text.length());
	}
	
	public void write(int position, String s){
		text.insert(position, s);

	}
	
	public void remove(int position){
		text.deleteCharAt(position);
	}
	
	public void remove(int start, int end){
		text.delete(start, end);
	}
	
	public StringBuffer getText() {
		return text;
	}
	
	public void setContent(String content) {
		clear();
		write(0, content);
	}

	public String toString() {
		return this.text.toString();
	}
	
	public int length() {
		return this.text.length();
	}
}
