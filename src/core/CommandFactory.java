package core;

import java.util.ArrayList;
import java.util.List;

import commands.Backward;
import commands.Command;
import commands.Copy;
import commands.Cut;
import commands.Forward;
import commands.MoveCursor;
import commands.Paste;
import commands.Redo;
import commands.Undo;
import commands.Write;

public class CommandFactory {
	
	private Context ctx;
	
	public CommandFactory(Context ctx) {
		this.ctx = ctx;
	}
	
	public List<Command> create(List<String> commands) {
		List<Command> res = new ArrayList<>();
		for(String cmdName : commands) {
			res.add(create(cmdName));
		}
		return res;
	}

	public Command create(String cmdName) {
		switch(cmdName) {
			case "Backward" :
				return new Backward(ctx);
			case "Copy" :
				return new Copy(ctx);
			case "Cut" :
				return new Cut(ctx);
			case "Forward" :
				return new Forward(ctx);
			case "Paste" :
				return new Paste(ctx);
			case "Undo" :
				return new Undo(ctx);
			case "Redo" :
				return new Redo(ctx);
			case "Write" :
				return new Write(ctx, "");
			case "MoveCursor" :
				return new MoveCursor(ctx, 0);
			default : // Throw exception ?
				return null;
		}
	}

	public Command createWrite(String text) {
		return new Write(ctx, text);
	}

	public Command createMoveCursor(int position) {
		return new MoveCursor(ctx, position);
	}

}
