package core;

public class Selector {

	private int cursorPosition;
	private int length;
	
	public Selector() {
		cursorPosition = 0;
		length = 0;
	}
	
	public Integer getCursorPosition() {
		return this.cursorPosition;
	}
	
	public void setCursorPosition(int position) {
		this.cursorPosition = position;
		this.length = 0;
	}
	
	public Integer getLength() {
		return this.length;
	}
	
	public String render(StringBuffer input) {
		Pair<Integer, Integer> p = getStartAndEnd();
		return input.substring(p.getFirst(), p.getSecond());
	}
	
	public Pair<Integer, Integer> getStartAndEnd() {
		int start = getCursorPosition();
		int end = getCursorPosition() + getLength();
		
		if (start > end) {
			int tmp = start;
			start = end;
			end = tmp;
		}

		return new Pair<Integer, Integer>(start, end);
	}
	
	public void setSelection(int start, int length) {
		cursorPosition = start;
		this.length = length;
	}
}
