package core;

import invokers.Invoker;
import momento.Momento;
import momento.Originator;

public class Context  implements Originator{
	
	private Buffer buffer;
	private Selector selection;
	private UndoRedoStack undoRedoStack;
	private Invoker invoker;
	
	public Context(Invoker invoker, UndoRedoStack undoRedoStack) {
		buffer = new Buffer();
		selection = new Selector();
		this.undoRedoStack = undoRedoStack;
		this.invoker = invoker;
	}
	
	public Context(Invoker invoker) {
		this(invoker, new UndoRedoStack());	
	}
	
	public UndoRedoStack getUndoRedoStack() {
		return undoRedoStack;
	}
	
	public Invoker getInvoker() {
		return this.invoker;
	}
	
	public void setSelection(int start, int length) {
		this.selection.setSelection(start, length);
	}
	
	public Selector getSelection() {
		return this.selection;
	}
	
	public int getCursorPosition() {
		return this.selection.getCursorPosition();
	}
	
	public String renderSelection() {
		return selection.render(getBuffer().getText());
	}
	
	public void setBufferContent(String content) {
		this.buffer.setContent(content);
		this.undoRedoStack.pushDoneCommand(new Momento(content));
	}
	
	public Buffer getBuffer() {
		return buffer;
	}

	public void eraseSelection() {
		Pair<Integer, Integer> p = this.selection.getStartAndEnd();
		this.buffer.remove(p.getFirst(), p.getSecond());		
	}

	@Override
	public Momento save() {
		return new Momento(getBuffer().toString());
	}

	@Override
	public void restore(Momento m) {
		if(m == null) {
			getBuffer().setContent("");
		}else {
			getBuffer().setContent(m.getState());
		}
	}
	
}
